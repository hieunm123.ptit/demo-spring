package com.ptit.demospring;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
@RestController
@Tag(name = "USERS")
@Slf4j
public class UserController {

    private final UserService userService;

    @PostMapping
    @Operation(summary = "Create a new user", responses = {
            @ApiResponse(description = "The created user", content = @Content(schema = @Schema(implementation = User.class))),
            @ApiResponse(responseCode = "400", description = "Invalid user details")
    })
    public ResponseEntity<User> create(@RequestBody UserRequest request) {

        log.info("(create) request:{}", request);

        return new ResponseEntity<>(userService.create(request), HttpStatus.CREATED);
    }

    @GetMapping
    @Operation(summary = "List all users")
    public ResponseEntity<List<User>> list() {

        log.info("(List all users)");

        return ResponseEntity.ok(userService.list());
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> detail(@PathVariable Integer id) {
        log.info("(detail) id:{}", id);
        return ResponseEntity.ok(userService.detail(id));
    }

    @PutMapping("/{id}")
    public ResponseEntity<User> update(@PathVariable Integer id, @RequestBody UserRequest request) {
        log.info("(update) id:{}, request:{}", id, request);
        return ResponseEntity.ok(userService.update(id, request));
    }
}
