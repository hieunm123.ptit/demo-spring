package com.ptit.demospring;


import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {

    User create(UserRequest request);

    User update(Integer id, UserRequest request);

    List<User> list();

    User detail(Integer id);

    void delete(Integer id);

}
