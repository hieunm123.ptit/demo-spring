package com.ptit.demospring;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository repository;

    @Override
    public User create(UserRequest request) {

        log.info("(create) request:{}", request);

        return repository.save(User.builder()
                .name(request.getName())
                .email(request.getEmail())
                .build());
    }

    @Override
    public User update(Integer id, UserRequest request) {
        log.info("(update) id:{}, request:{}", id, request);

        User user = repository.findById(id).orElseThrow();

        user.setName(request.getName());
        user.setEmail(request.getEmail());

        return repository.save(user);
    }

    @Override
    public List<User> list() {
        log.info("(List all users)");
        return repository.findAll();
    }

    @Override
    public User detail(Integer id) {
        log.info("(detail) id:{}", id);
        return repository.findById(id).orElseThrow();
    }

    @Override
    public void delete(Integer id) {
        log.info("(delete) id:{}", id);
        repository.deleteById(id);
    }

}
